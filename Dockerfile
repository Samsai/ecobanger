FROM clux/muslrust as build-stage

COPY . /usr/src

WORKDIR /usr/src

RUN cargo clean && cargo build --release

FROM alpine

COPY --from=build-stage /usr/src/ /usr/app
WORKDIR /usr/app

CMD ["./target/x86_64-unknown-linux-musl/release/ecobanger"]
