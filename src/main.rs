#![feature(proc_macro_hygiene, decl_macro)]

#![macro_use] extern crate rocket;
extern crate rocket_contrib;

use rocket::{get, post, routes};
use rocket::http::RawStr;
use rocket::response::Redirect;
use rocket::response::status::BadRequest;
use rocket_contrib::templates::Template;

use std::collections::HashMap;

#[get("/")]
fn index() -> Template {
    let context: HashMap<String, String> = HashMap::new();
    Template::render("index", &context)
}

#[get("/search?<q>")]
fn search(q: &RawStr) -> Result<Redirect, BadRequest<&str>> {
    if let Ok(query) = q.url_decode() {
        if query.starts_with("!") {
            return Ok(Redirect::to(format!("https://duckduckgo.com/?q={}", q)));
        } else {
            return Ok(Redirect::to(format!("https://ecosia.org/search?q={}", q)));
        }
    } else {
        return Err(BadRequest(Some("Malformed string")));
    }

}

fn main() {
    rocket::ignite()
        .attach(Template::fairing())
        .mount("/", routes![index, search]).launch();
}
